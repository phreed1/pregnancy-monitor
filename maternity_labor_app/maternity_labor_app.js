/* persistent global data stores */

EKG_list = new Mongo.Collection('ekg_detail');

if (Meteor.isClient) {
    // console.log("hey client");
    Template.ekg_history.helpers({
        'detail': function() {
            return EKG_list.find({}, {
                sort: {
                    name: 1,
                    reading: 1
                }
            });
        },
        'count': function() {
            return EKG_list.find().count();
        },
        'selected_class': function() {
            var current_patient_id = this._id;
            var selected_patient_id = Session.get('selected-patient');
            if (current_patient_id == selected_patient_id) {
                return "selected";
            }
        }
    });

    Template.ekg_history.events({
        'click .remove_patient': function(ev) {
            var selected_patient = Session.get('selected-patient');
            EKG_list.remove(selected_patient);
        },
        'click .patient_reading': function(ev) {
            var patient_id = this._id;
            Session.set('selected-patient', patient_id);
        },
        'dblclick .patient_reading': function(ev) {
            console.log("double c-li-cker");
        },
        'focus .patient_reading': function(ev) {
            console.log("patient focus");
        },
        'blur .patient_reading': function(ev) {
            console.log("patient blur");
        },
        'mouseover .patient_reading': function(ev) {
            console.log("patient moOse over");
        },
        'change .patient_reading': function(ev) {
            console.log("patient changer");
        },
        'click .next_state': function(ev) {
            var selected_patient_id = Session.get('selected-patient');
            var new_reading = d3.range(1,8).map(
                        function(x) { return Math.floor((Math.random() * 10) + 1); } );
            console.log("new reading");
            console.log(new_reading);

            EKG_list.update(selected_patient_id, {
                $set: { reading: new_reading }
            });
        }
    })
    Template.add_patient_form.events({
        'submit form' : function(ev) {
            ev.preventDefault();
            var patient_name_var = ev.target.patient_name.value;
             var new_reading = d3.range(1,8).map(
                        function(x) { return Math.floor((Math.random() * 10) + 1); } );
            EKG_list.insert({
                patient_id: patient_name_var,
                reading: new_reading});
        }
    })
}

if (Meteor.isServer) {
    console.log("hey server");
}

starter = function() {
    EKG_list.insert({
        uuid: "41655bac-96ea-4a20-bfbb-443a35934985",
        creation: "2015-05-15:11:19:00.001",
        patient_id: "431c3043-fd13-4c60-a020-06a8917a996d",
        reading: [56, 37, 29, 67, 76, 82, 77, 42]
    });
    EKG_list.insert({
        uuid: "41655bac-96ea-4a20-bfbb-443a35934985",
        creation: "2015-05-15:11:19:00.001",
        patient_id: "Samuel",
        reading: [56, 37, 29, 67, 76, 82, 77, 42]
    });
    EKG_list.insert({
        uuid: "41655bac-96ea-4a20-bfbb-443a35934985",
        creation: "2015-05-15:11:19:00.001",
        patient_id: "Mary",
        reading: [56, 37, 29, 67, 76, 82, 77, 42]
    });
}
